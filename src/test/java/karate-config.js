function fn() {
  var env = karate.env; // get system property 'karate.env'
  karate.log('karate.env system property was:', env);
  karate.configure('ssl', true);
  karate.configure('connectTimeout', 60000);
  karate.configure('readTimeout', 60000);
  if (!env) {
    env = 'dev';
  }
  var config = {
    env: env,
    myVarName: 'someValue'
    apiVersion: 'v1.0',
    aisProductId: '700',
    kiwisaverProductId: '701',
    nzrtProductId: '702',
    aisUrl: 'https://api-ig.asnp.nzwm.nz',
    kiwisaverUrl: 'https://api-ig.ksnp.nzwm.nz',
    nzrtUrl: 'https://api-ig.rtnp.nzwm.nz'
  }
  if (env == 'dev') {
    // customize
    // e.g. config.foo = 'bar';
  } else if (env == 'e2e') {
    // customize
  }
  return config;
}