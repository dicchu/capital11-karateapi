package utility;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Database {

    private static String jdbcClassName = "com.ibm.db2.jcc.DB2Driver";
    private static String user = "autouser";
    private static String password = "Automat1on";
    private static Map<String, String> urlMap = new HashMap<>() {
        {
            put("CPAIUT11","jdbc:db2://10.182.13.95:50006/CPAIUT11");
            put("CPKSUT11","jdbc:db2://10.182.31.103:50004/CPKSUT11");
            put("CPRTUT11","jdbc:db2://10.182.38.159:50002/CPRTUT11");
        }
    };

    public static List getDbData(String alias, String sql) {

        Connection connection = null;
        Statement statement = null;
        ResultSet resultSet = null;
        ResultSetMetaData metaData = null;
        int columns = 0;
        List<Map<String, Object>> result = new ArrayList<>();

        try {
            //Load class into memory
            Class.forName(jdbcClassName);

            //Establish connection
            connection = DriverManager.getConnection(urlMap.get(alias), user, password);

            //Create a statement
            statement = connection.createStatement();

            //Execute query and generate ResultSet
            resultSet = statement.executeQuery(sql);
            metaData = resultSet.getMetaData();
            columns = metaData.getColumnCount();

            while (resultSet.next()) {
                Map<String, Object> row = new HashMap<>(columns);
                for (int i = 1; i <= columns; ++i) {
                    row.put(metaData.getColumnName(i), resultSet.getObject(i));
                }
                result.add(row);
            }

            resultSet.close();
            connection.close();
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        } finally {
            if(connection!=null){
                System.out.println("Connected successfully.");
            } else {
                System.out.println("Connection failed.");
            }
        }

        return result;
    }
}
